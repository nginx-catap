#!/bin/bash
test -d .git || exit 0
git --version 2>/dev/null 1>/dev/null || exit 0
git rev-list HEAD | sort > config.git-hash
LOCALVER=`wc -l config.git-hash | awk '{print $1}'`
if [ $LOCALVER \> 1 ] ; then
    VER=`git rev-list origin/master | sort | join config.git-hash - | wc -l | awk '{print $1}'`
    if [ $VER != $LOCALVER ] ; then
        VER="$VER+$(($LOCALVER-$VER))"
    elif git status | grep -q "modified:" ; then
        VER="${VER}M"
    fi
    VER="$VER.$(git rev-list HEAD -n 1 | head -c 7)"
else
    VER="x"
fi
rm -f config.git-hash
sed -i -e "s:\"\([0-9]*\.[0-9]*\.[0-9]*\).*\".*$:\"\1\.$VER\":g" \
    -e "s:\"nginx/\":\"nginx-catap/\":g" \
    src/core/nginx.h
sed -i -e "s:'\([0-9]*\.[0-9]*\.[0-9]*\).*'.*$:\'\1\.$VER\';:g" \
    src/http/modules/perl/nginx.pm
