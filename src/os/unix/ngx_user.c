
/*
 * Copyright (C) Igor Sysoev
 */


#include <ngx_config.h>
#include <ngx_core.h>

#if (NGX_HAVE_CAPABILITIES)
#include <sys/prctl.h>
#endif

#if (NGX_HAVE_SYSINFO)
#include <sys/sysinfo.h>
#endif

/*
 * Solaris has thread-safe crypt()
 * Linux has crypt_r(); "struct crypt_data" is more than 128K
 * FreeBSD needs the mutex to protect crypt()
 *
 * TODO:
 *     ngx_crypt_init() to init mutex
 */


#if (NGX_CRYPT)

#if (NGX_HAVE_GNU_CRYPT_R)

ngx_int_t
ngx_crypt(ngx_pool_t *pool, u_char *key, u_char *salt, u_char **encrypted)
{
    char               *value;
    size_t              len;
    ngx_err_t           err;
    struct crypt_data   cd;

    ngx_set_errno(0);

    cd.initialized = 0;
    /* work around the glibc bug */
    cd.current_salt[0] = ~salt[0];

    value = crypt_r((char *) key, (char *) salt, &cd);

    err = ngx_errno;

    if (err == 0) {
        len = ngx_strlen(value);

        *encrypted = ngx_pnalloc(pool, len);
        if (*encrypted) {
            ngx_memcpy(*encrypted, value, len + 1);
            return NGX_OK;
        }
    }

    ngx_log_error(NGX_LOG_CRIT, pool->log, err, "crypt_r() failed");

    return NGX_ERROR;
}

#else

ngx_int_t
ngx_crypt(ngx_pool_t *pool, u_char *key, u_char *salt, u_char **encrypted)
{
    char       *value;
    size_t      len;
    ngx_err_t   err;

#if (NGX_THREADS && NGX_NONREENTRANT_CRYPT)

    /* crypt() is a time consuming funtion, so we only try to lock */

    if (ngx_mutex_trylock(ngx_crypt_mutex) != NGX_OK) {
        return NGX_AGAIN;
    }

#endif

    ngx_set_errno(0);

    value = crypt((char *) key, (char *) salt);

    if (value) {
        len = ngx_strlen(value);

        *encrypted = ngx_pnalloc(pool, len);
        if (*encrypted) {
            ngx_memcpy(*encrypted, value, len + 1);
        }

#if (NGX_THREADS && NGX_NONREENTRANT_CRYPT)
        ngx_mutex_unlock(ngx_crypt_mutex);
#endif
        return NGX_OK;
    }

    err = ngx_errno;

#if (NGX_THREADS && NGX_NONREENTRANT_CRYPT)
    ngx_mutex_unlock(ngx_crypt_mutex);
#endif

    ngx_log_error(NGX_LOG_CRIT, pool->log, err, "crypt() failed");

    return NGX_ERROR;
}

#endif

#endif /* NGX_CRYPT */

ngx_int_t ngx_switch_user(ngx_cycle_t *cycle)
{
    ngx_core_conf_t  *ccf;

    ccf = (ngx_core_conf_t *) ngx_get_conf(cycle->conf_ctx, ngx_core_module);

    if (geteuid()) {
        return NGX_DONE;
    }

    if (setgid(ccf->group) == -1) {
        ngx_log_error(NGX_LOG_EMERG, cycle->log, ngx_errno,
                      "setgid(%d) failed", ccf->group);
        return NGX_ERROR;
    }

    if (initgroups(ccf->username, ccf->group) == -1) {
        ngx_log_error(NGX_LOG_EMERG, cycle->log, ngx_errno,
                      "initgroups(%s, %d) failed",
                      ccf->username, ccf->group);
        return NGX_ERROR;
    }

    if (setuid(ccf->user) == -1) {
        ngx_log_error(NGX_LOG_EMERG, cycle->log, ngx_errno,
                      "setuid(%d) failed", ccf->user);
        return NGX_ERROR;
    }

    return NGX_OK;
}

#if (NGX_HAVE_CAPABILITIES)

ngx_int_t ngx_capabilities_prepare(ngx_log_t *log)
{
    if (geteuid()) {
        return NGX_DONE;
    }

    if (prctl(PR_SET_KEEPCAPS, 1, 0, 0, 0) == -1) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "prctl(PR_SET_KEEPCAPS, 1) failed");
        return NGX_ERROR;
    }

    return NGX_OK;
}


ngx_int_t ngx_set_capability(ngx_log_t *log, ngx_int_t ncap, cap_value_t *cap_list)
{
    cap_t  caps;

    caps = cap_get_proc();
    if (caps == NULL) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "cap_get_proc() failed");
        return NGX_ERROR;
    }

    if (cap_set_flag(caps, CAP_EFFECTIVE, ncap, cap_list, CAP_SET) == -1) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "cap_set_flag(CAP_EFFECTIVE) failed");
        return NGX_ERROR;
    }

    if (cap_set_flag(caps, CAP_PERMITTED, ncap, cap_list, CAP_SET) == -1) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "cap_set_flag(CAP_PERMITTED) failed");
        return NGX_ERROR;
    }

    if (cap_set_flag(caps, CAP_INHERITABLE, ncap, cap_list, CAP_SET) == -1) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "cap_set_flag(CAP_INHERITABLE) failed");
        return NGX_ERROR;
    }

    if (cap_set_proc(caps) == -1) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "cap_set_proc() failed");
        return NGX_ERROR;
    }

    if (cap_free(caps) == -1) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "cap_free() failed");
        return NGX_ERROR;
    }

    return NGX_OK;
}


ngx_int_t ngx_clear_capability(ngx_log_t *log)
{
    cap_t      caps;

    caps = cap_get_proc();
    if (caps == NULL) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "cap_get_proc() failed");
        return NGX_ERROR;
    }

    if (cap_clear(caps) == -1) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "cap_clear() failed");
        return NGX_ERROR;
    }

    if (cap_set_proc(caps) == -1) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "cap_set_proc() failed");
        return NGX_ERROR;
    }

    if (cap_free(caps) == -1) {
        ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                      "cap_free() failed");
        return NGX_ERROR;
    }

    return NGX_OK;
}

#endif


ngx_int_t ngx_get_loadavg(ngx_uint_t n)
{
#if (NGX_HAVE_SYSINFO)
    struct sysinfo s;

    if (sysinfo(&s)) {
	goto error;
    }

    return s.loads[n]/65536;

#endif /* NGX_HAVE_SYSINFO */

#if (NGX_HAVE_GETLOADAVG)
    double loadavg[1];

    if (getloadavg(loadavg, 1) == -1) {
	goto error;
    }

    return (int)loadavg[n];

#endif /* NGX_HAVE_SYSINFO */

error:
    return NGX_ERROR;
}
