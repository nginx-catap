
/*
 * Copyright (C) Igor Sysoev
 */


#include <ngx_config.h>
#include <ngx_core.h>


/*
 * FreeBSD does not test /etc/localtime change, however, we can workaround it
 * by calling tzset() with TZ and then without TZ to update timezone.
 * The trick should work since FreeBSD 2.1.0.
 *
 * Linux does not test /etc/localtime change in localtime(),
 * but may stat("/etc/localtime") several times in every strftime(),
 * therefore we use it to update timezone.
 *
 * Solaris does not test /etc/TIMEZONE change too and no workaround available.
 */

void
ngx_timezone_update(void)
{
#if (NGX_FREEBSD)

    if (getenv("TZ")) {
        return;
    }

    putenv("TZ=UTC");

    tzset();

    unsetenv("TZ");

    tzset();

#elif (NGX_LINUX)
    time_t      s;
    struct tm  *t;
    char        buf[4];

    s = time(0);

    t = localtime(&s);

    strftime(buf, 4, "%H", t);

#endif
}


void
ngx_localtime(time_t s, ngx_tm_t *tm)
{
#if (NGX_HAVE_LOCALTIME_R)
    (void) localtime_r(&s, tm);

#else
    ngx_tm_t  *t;

    t = localtime(&s);
    *tm = *t;

#endif

    tm->ngx_tm_mon++;
    tm->ngx_tm_year += 1900;
}


void
ngx_libc_localtime(time_t s, struct tm *tm)
{
#if (NGX_HAVE_LOCALTIME_R)
    (void) localtime_r(&s, tm);

#else
    struct tm  *t;

    t = localtime(&s);
    *tm = *t;

#endif
}


void
ngx_libc_gmtime(time_t s, struct tm *tm)
{
#if (NGX_HAVE_LOCALTIME_R)
    (void) gmtime_r(&s, tm);

#else
    struct tm  *t;

    t = gmtime(&s);
    *tm = *t;

#endif
}

#if (NGX_CLOCK)
ngx_timespec_t
ngx_timespec_diff(ngx_timespec_t *start, ngx_timespec_t *end)
{
    ngx_timespec_t temp;
    if ((end->tv_nsec - start->tv_nsec) < 0) {
	temp.tv_sec = end->tv_sec - start->tv_sec-1;
	temp.tv_nsec = 1000000000 + end->tv_nsec - start->tv_nsec;
    } else {
	temp.tv_sec = end->tv_sec - start->tv_sec;
	temp.tv_nsec = end->tv_nsec - start->tv_nsec;
    }
    return temp;
}

ngx_int_t
ngx_clock_gettime(ngx_clockid_t id, ngx_timespec_t *res)
{
    return (ngx_int_t)clock_gettime((clockid_t)id, (struct timespec*)res);
}

#if (NGX_HAVE_CLOCK_PROFILE)
ngx_timeval_t       ngx_gettimeofday_start_tv = {0, 0};

static void
ngx_gettimeofday_init()
{
    ngx_timespec_t      ts;
    ngx_timeval_t       tv;

    gettimeofday(&tv, NULL);
    ngx_clock_gettime(NGX_CLOCK_PROFILE, &ts);

    if ((tv.tv_usec - ts.tv_nsec/1000) < 0) {
        ngx_gettimeofday_start_tv.tv_sec = tv.tv_sec - 1;
        ngx_gettimeofday_start_tv.tv_usec = 1000000 + tv.tv_usec - ts.tv_nsec/1000;
    } else {
        ngx_gettimeofday_start_tv.tv_sec = tv.tv_sec;
        ngx_gettimeofday_start_tv.tv_usec = tv.tv_usec - ts.tv_nsec/1000;
    }
}

void ngx_gettimeofday(ngx_timeval_t *tp)
{
    ngx_timespec_t      ts;

    if (ngx_gettimeofday_start_tv.tv_sec == 0 &&
        ngx_gettimeofday_start_tv.tv_usec == 0) {
	ngx_gettimeofday_init();
    }

    ngx_clock_gettime(NGX_CLOCK_PROFILE, &ts);

    if ((ts.tv_nsec/1000 + ngx_gettimeofday_start_tv.tv_usec) > 1000000) {
        tp->tv_sec = ngx_gettimeofday_start_tv.tv_sec + 1;
        tp->tv_sec = ngx_gettimeofday_start_tv.tv_usec + ts.tv_nsec/1000 - 1000000;
    } else {
        tp->tv_sec = ngx_gettimeofday_start_tv.tv_sec;
        tp->tv_usec = ngx_gettimeofday_start_tv.tv_usec + ts.tv_nsec/1000;
    }
}
#endif

#endif
