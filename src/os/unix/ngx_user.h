
/*
 * Copyright (C) Igor Sysoev
 */


#ifndef _NGX_USER_H_INCLUDED_
#define _NGX_USER_H_INCLUDED_


#include <ngx_config.h>
#include <ngx_core.h>

#if (NGX_HAVE_CAPABILITIES)
#include <sys/capability.h>
#endif


typedef uid_t  ngx_uid_t;
typedef gid_t  ngx_gid_t;

#if (NGX_CRYPT)

#if (NGX_HAVE_GNU_CRYPT_R)


ngx_int_t ngx_crypt(ngx_pool_t *pool, u_char *key, u_char *salt,
    u_char **encrypted);

#endif

#endif /* NGX_CRYPT */

ngx_int_t ngx_switch_user(ngx_cycle_t *cycle);

#if (NGX_HAVE_CAPABILITIES)

ngx_int_t ngx_capabilities_prepare(ngx_log_t *log);

ngx_int_t ngx_set_capability(ngx_log_t *log, ngx_int_t ncap, cap_value_t *cap_list);

ngx_int_t ngx_clear_capability(ngx_log_t *log);

#endif

ngx_int_t ngx_get_loadavg(ngx_uint_t n);
#endif /* _NGX_USER_H_INCLUDED_ */
