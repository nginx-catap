
/*
 * Copyright (C) Igor Sysoev
 */


#ifndef _NGX_TIME_H_INCLUDED_
#define _NGX_TIME_H_INCLUDED_


#include <ngx_config.h>
#include <ngx_core.h>


typedef ngx_rbtree_key_t      ngx_msec_t;
typedef ngx_rbtree_key_int_t  ngx_msec_int_t;

typedef struct tm             ngx_tm_t;
typedef struct timeval        ngx_timeval_t;

#define ngx_tm_sec            tm_sec
#define ngx_tm_min            tm_min
#define ngx_tm_hour           tm_hour
#define ngx_tm_mday           tm_mday
#define ngx_tm_mon            tm_mon
#define ngx_tm_year           tm_year
#define ngx_tm_wday           tm_wday
#define ngx_tm_isdst          tm_isdst

#define ngx_tm_sec_t          int
#define ngx_tm_min_t          int
#define ngx_tm_hour_t         int
#define ngx_tm_mday_t         int
#define ngx_tm_mon_t          int
#define ngx_tm_year_t         int
#define ngx_tm_wday_t         int


#if (NGX_HAVE_GMTOFF)
#define ngx_tm_gmtoff         tm_gmtoff
#define ngx_tm_zone           tm_zone
#endif


#if (NGX_SOLARIS)

#define ngx_timezone(isdst) (- (isdst ? altzone : timezone) / 60)

#else

#define ngx_timezone(isdst) (- (isdst ? timezone + 3600 : timezone) / 60)

#endif


void ngx_timezone_update(void);
void ngx_localtime(time_t s, ngx_tm_t *tm);
void ngx_libc_localtime(time_t s, struct tm *tm);
void ngx_libc_gmtime(time_t s, struct tm *tm);


#if (NGX_CLOCK)

typedef struct timespec ngx_timespec_t;
typedef clockid_t       ngx_clockid_t;

#if (NGX_HAVE_CLOCK_REALTIME)
#define NGX_CLOCK_REALTIME           CLOCK_REALTIME
#endif	/* NGX_HAVE_CLOCK_REALTIME */

#if (NGX_HAVE_CLOCK_VIRTUAL)
#define NGX_CLOCK_VIRTUAL            CLOCK_VIRTUAL
#endif	/* NGX_HAVE_CLOCK_VIRTUAL */

#if (NGX_HAVE_CLOCK_MONOTONIC)
#define NGX_CLOCK_MONOTONIC          CLOCK_MONOTONIC
#endif	/* NGX_HAVE_CLOCK_MONOTONIC */

#if (NGX_HAVE_CLOCK_PROCESS_CPUTIME_ID)
#define NGX_CLOCK_PROCESS_CPUTIME_ID CLOCK_PROCESS_CPUTIME_ID
#endif	/* NGX_HAVE_CLOCK_PROCESS_CPUTIME_ID */

#if (NGX_HAVE_CLOCK_THREAD_CPUTIME_ID)
#define NGX_CLOCK_THREAD_CPUTIME_ID  CLOCK_THREAD_CPUTIME_ID
#endif	/* NGX_HAVE_CLOCK_THREAD_CPUTIME_ID */

#if (NGX_HAVE_CLOCK_PROFILE)
#define NGX_CLOCK_PROFILE            CLOCK_PROFILE
#endif	/* NGX_HAVE_CLOCK_PROFILE */

#if (NGX_HAVE_CLOCK_PROF)
#define NGX_CLOCK_PROFILE            CLOCK_PROF
#endif	/* NGX_HAVE_CLOCK_PROF */

#if (NGX_HAVE_CLOCK_UPTIME)
#define NGX_CLOCK_UPTIME            CLOCK_UPTIME
#endif	/* NGX_HAVE_CLOCK_PROF */

ngx_timespec_t ngx_timespec_diff(ngx_timespec_t *start, ngx_timespec_t *end);
ngx_int_t ngx_clock_gettime(ngx_clockid_t id, ngx_timespec_t *res);

#endif /* NGX_CLOCK */

#if (NGX_CLOCK) && (NGX_HAVE_CLOCK_PROFILE)
extern ngx_timespec_t      ngx_gettimeofday_start_ts;

void ngx_gettimeofday(ngx_timeval_t *tp);
#else
#define ngx_gettimeofday(tp)  (void) gettimeofday(tp, NULL);
#endif	/* (NGX_CLOCK) && (NGX_HAVE_CLOCK_MONOTONIC) */

#define ngx_msleep(ms)        (void) usleep(ms * 1000)
#define ngx_sleep(s)          (void) sleep(s)


#endif /* _NGX_TIME_H_INCLUDED_ */
