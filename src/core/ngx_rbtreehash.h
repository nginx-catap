
/*
 * Copyright (C) Kirill A. Korinskiy
 */

#ifndef __NGX_RBTREEHASH
#define __NGX_RBTREEHASH

#include <ngx_config.h>
#include <ngx_core.h>

typedef struct ngx_rbtreehash_list_node_s ngx_rbtreehash_list_node_t;

struct ngx_rbtreehash_list_node_s {
    size_t    len;
    void     *data;

    ngx_rbtreehash_list_node_t *next;
    ngx_rbtreehash_list_node_t *prev;
};

typedef struct {
    ngx_rbtreehash_list_node_t *head;
    ngx_rbtreehash_list_node_t *tail;
} ngx_rbtreehash_list_t;


typedef struct {
    /* in hash using shm_zone */
    ngx_shm_zone_t     *shm_zone;
    ngx_str_t           shm_key;

    /* ... or pool */
    ngx_pool_t         *pool;

    ngx_log_t          *log;
} ngx_rbtreehash_pool_t;

typedef struct {
    ngx_rbtree_t         *tree;
    size_t                nodes;

    ngx_rbtreehash_list_t list;
} ngx_rbtreehash_hash_t;

typedef struct ngx_rbtreehash_s ngx_rbtreehash_t;

struct ngx_rbtreehash_s {
    ngx_rbtreehash_pool_t  pool;
    ngx_rbtreehash_hash_t *data;

    /* hack to pointer to next/prev config */
    ngx_rbtreehash_t      *next;
    ngx_rbtreehash_t      *prev;
};

ngx_int_t ngx_rbtreehash_init(ngx_rbtreehash_t *hash);
ngx_int_t ngx_rbtreehash_destroy(ngx_rbtreehash_t *hash);
ngx_rbtree_node_t* ngx_rbtreehash_insert(ngx_rbtreehash_t *hash, ngx_str_t *key,
					 void *value, size_t len);
ngx_int_t ngx_rbtreehash_delete(ngx_rbtreehash_t *hash, ngx_str_t *key);
void *ngx_rbtreehash_find(ngx_rbtreehash_t *hash, ngx_str_t *key, size_t *len);

char *ngx_rbtreehash_crete_shared_by_size(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);
char *ngx_rbtreehash_from_path(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);

ngx_int_t ngx_rbtreehash_merge_value(ngx_rbtreehash_t *conf, ngx_rbtreehash_t *prev);

extern ngx_module_t  ngx_rbtreehash_module;

#endif //__NGX_RBTREEHASH

/* Local Variables: */
/* mode: c */
/* c-basic-offset: 4 */
/* c-file-offsets: ((arglist-cont-nonempty . 4)) */
/* End: */
