
/*
 * Copyright (C) Kirill A. Korinskiy
 */

#ifndef _NGX_FLATHASH_H_INCLUDED_
#define _NGX_FLATHASH_H_INCLUDED_

#include <ngx_config.h>
#include <ngx_core.h>

typedef struct {
    size_t       value_len;
    size_t       length;
    u_char       bits;
    u_char       data[1];
} ngx_flathash_t;

void *ngx_flathash_get(ngx_flathash_t *hashtable, ngx_str_t *key);

size_t ngx_flathash_need_memory(size_t length, size_t size);

ngx_int_t ngx_flathash_init(ngx_flathash_t *hashtable, size_t length, size_t size);


#endif /* _NGX_FLATHASH_H_INCLUDED_ */
