
/*
 * Copyright (C) Kirill A. Korinskiy
 */


#include <ngx_config.h>
#include <ngx_core.h>


static void *ngx_status_module_create_conf(ngx_cycle_t *cycle);
static char *ngx_status_module_init_conf(ngx_cycle_t *cycle, void *conf);

static ngx_int_t ngx_status_module_init(ngx_cycle_t *cycle);

typedef struct {
    ngx_uint_t          window;
    ngx_uint_t          count;
    ngx_status_list_t **counters;
} ngx_status_conf_t;


static ngx_command_t  ngx_status_commands[] = {

    { ngx_string("status_window"),
      NGX_MAIN_CONF|NGX_DIRECT_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_num_slot,
      0,
      offsetof(ngx_status_conf_t, window),
      NULL },

      ngx_null_command
};


static ngx_core_module_t  ngx_status_module_ctx = {
    ngx_string("ngx_status"),
    ngx_status_module_create_conf,
    ngx_status_module_init_conf
};


ngx_module_t  ngx_status_module = {
    NGX_MODULE_V1,
    &ngx_status_module_ctx,     /* module context */
    ngx_status_commands,        /* module directives */
    NGX_CORE_MODULE,            /* module type */
    NULL,                       /* init master */
    ngx_status_module_init,     /* init module */
    NULL,                       /* init process */
    NULL,                       /* init thread */
    NULL,                       /* exit thread */
    NULL,                       /* exit process */
    NULL,                       /* exit master */
    NGX_MODULE_V1_PADDING
};


ngx_int_t
ngx_status_add_counter(ngx_cycle_t *cycle, ngx_module_t *module,
    ngx_status_counter_t *counter)
{
    ngx_status_conf_t *conf;
    ngx_status_list_t *c, *cc;

    conf = (ngx_status_conf_t *) ngx_get_conf(cycle->conf_ctx, ngx_status_module);

    c = ngx_palloc(cycle->pool, sizeof(ngx_status_list_t));
    if (c == NULL) {
        return NGX_ERROR;
    }

    c->counter = counter;
    c->next = NULL;

    if (conf->counters[module->index] == NULL) {
        conf->counters[module->index] = c;
    } else {
        cc = conf->counters[module->index];

        for (; cc->next; cc = cc->next);

        cc->next = c;
    }

    conf->count++;

    return NGX_OK;
}


ngx_int_t
ngx_status_add_child(ngx_cycle_t *cycle, ngx_status_counter_t *parent,
    ngx_status_counter_t *child)
{
    ngx_status_conf_t *conf;
    ngx_status_list_t *c, *cc;

    conf = (ngx_status_conf_t *) ngx_get_conf(cycle->conf_ctx, ngx_status_module);

    c = ngx_palloc(cycle->pool, sizeof(ngx_status_list_t));
    if (c == NULL) {
        return NGX_ERROR;
    }

    c->counter = child;
    c->next = NULL;

    if (parent->childs == NULL) {
        parent->childs = c;
    } else {
        cc = parent->childs;

        for (; cc->next; cc = cc->next);

        cc->next = c;
    }

    conf->count++;

    return NGX_OK;
}


ngx_int_t
ngx_status_fetch_add(ngx_status_counter_t *counter)
{
    ngx_atomic_t      *window;
    ngx_status_conf_t *conf;

    conf = (ngx_status_conf_t *) ngx_get_conf(ngx_cycle->conf_ctx,
                                              ngx_status_module);

    window = counter->windows + (ngx_time() % (2 * conf->window));

    ngx_atomic_fetch_add(window, 1);

    if (window - conf->window > counter->windows) {
        *(window - conf->window) = 0;
    } else {
        *(window + conf->window) = 0;
    }

    ngx_atomic_fetch_add(counter->accumulate, 1);

    return NGX_OK;
}


ngx_int_t
ngx_status_get_accumulate_value(ngx_status_counter_t *counter)
{
    return *(counter->accumulate);
}


ngx_int_t
ngx_status_get_periodic_value(ngx_status_counter_t *counter, ngx_uint_t period)
{
    ngx_int_t          sum ;
    ngx_atomic_t      *start, *last;
    ngx_status_conf_t *conf;


    conf = (ngx_status_conf_t *)
        ngx_get_conf(ngx_cycle->conf_ctx, ngx_status_module);

    if (period > conf->window) {
        period = conf->window;
    }

    sum = 0;
    last = counter->windows + 2 * conf->window;
    start = counter->windows + (ngx_time() % (2 * conf->window));

    for (; period; period--) {
        sum += *start;
        if (start - 1 < counter->windows) {
            start = last;
        } else {
            start--;
        }
    }

    return sum;
}


ngx_status_list_t **
ngx_status_get_counters(ngx_cycle_t *cycle)
{
    return ((ngx_status_conf_t *)
            ngx_get_conf(cycle->conf_ctx, ngx_status_module))->counters;
}


static void *
ngx_status_module_create_conf(ngx_cycle_t *cycle)
{
    ngx_status_conf_t *scf;
    ngx_uint_t         i;

    scf = ngx_pcalloc(cycle->pool, sizeof(ngx_status_conf_t));
    if (scf == NULL) {
        return NULL;
    }

    scf->window = NGX_CONF_UNSET_UINT;

    for (i = 0; ngx_modules[i]; i++);

    scf->counters = ngx_pcalloc(cycle->pool, sizeof(ngx_status_list_t) * i);
    if (scf->counters == NULL) {
        return NULL;
    }

    scf->count = 0;

    return scf;
}


static char *
ngx_status_module_init_conf(ngx_cycle_t *cycle, void *conf)
{
    ngx_status_conf_t  *scf = conf;

    ngx_conf_init_uint_value(scf->window, 300);

    return NGX_CONF_OK;
}

static ngx_uint_t
ngx_status_set_associating_memory(u_char *shared, size_t cl,
    ngx_uint_t count, ngx_uint_t window, ngx_status_list_t *c, ngx_uint_t j)
{
    while (c) {
        c->counter->accumulate = (ngx_atomic_t *)(shared + (j * cl));
        c->counter->windows = (ngx_atomic_t *)(shared + (count * cl)
                                               + (2 * j * cl * window));

        j = ngx_status_set_associating_memory(shared, cl, count, window,
                                              c->counter->childs, j + 1);

        c = c->next;
    }

    return j;
}


static ngx_int_t
ngx_status_module_init(ngx_cycle_t *cycle)
{
    u_char            *shared;
    size_t             size, cl;
    ngx_shm_t          shm;
    ngx_uint_t         i, j;
    ngx_status_conf_t *conf;

    conf = (ngx_status_conf_t *) ngx_get_conf(cycle->conf_ctx, ngx_status_module);

    if (!conf->count) {
        return NGX_OK;
    }

    /* cl should be equal or bigger than cache line size */
    cl = 128;

    size = conf->count * cl + 2 * conf->window * conf->count * cl;

    shm.size = size;
    shm.name.len = sizeof("nginx_status_shared_zone");
    shm.name.data = (u_char *) "nginx_status_shared_zone";
    shm.log = cycle->log;

    if (ngx_shm_alloc(&shm) != NGX_OK) {
        return NGX_ERROR;
    }

    shared = shm.addr;

    for (i = 0, j = 0; ngx_modules[i]; i++) {
        j = ngx_status_set_associating_memory(shared, cl, conf->count,
                conf->window, conf->counters[ngx_modules[i]->index], j);
    }

    return NGX_OK;
}
