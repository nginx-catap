
/*
 * Copyright (C) Kirill A. Korinskiy
 */

#include <ngx_config.h>
#include <ngx_core.h>


typedef struct {
    u_char     data[1];
} ngx_flathash_node_t;


#define ngx_flathash_hashsize(n) ((uint32_t)1 << (n))
#define ngx_flathash_hashmask(n) (ngx_flathash_hashsize(n) - 1)
#define ngx_flathash_hashfunc(a, b) ngx_lookup3_hashlittle(a, b, 0x715517)


/*
 * Really simple index
 */
static inline ngx_flathash_node_t *
ngx_flathash_index(ngx_flathash_t *hashtable, uint32_t hash) {
    return (ngx_flathash_node_t *)(hashtable->data +
                                   ((offsetof(ngx_flathash_node_t, data)
                                     + hashtable->value_len)
                                    * hash));
};


void *
ngx_flathash_get(ngx_flathash_t *hashtable, ngx_str_t *key)
{
    uint32_t              hash;
    ngx_flathash_node_t  *rn;

    hash = ngx_flathash_hashfunc(key->data, key->len)
	& ngx_flathash_hashmask(hashtable->bits);

    rn = ngx_flathash_index(hashtable, hash);

    return rn->data;
}


size_t
ngx_flathash_need_memory(size_t length, size_t size)
{
    uint32_t    bits;
    size_t      i;

    for (bits = 0, i = size; i; i >>= 1, bits++);

    bits += 2;

    return offsetof(ngx_flathash_t, data)
	+ ((offsetof(ngx_flathash_node_t, data)
	    + length)
	   * 1<<bits);
}


ngx_int_t
ngx_flathash_init(ngx_flathash_t *hashtable, size_t length, size_t size)
{
    size_t      i;

    hashtable->value_len = length;

    hashtable->bits = 0;

    for (i = size; i; i >>= 1, hashtable->bits++);

    hashtable->bits += 2;

    hashtable->length = 1 << hashtable->bits;


    ngx_memzero(hashtable->data, hashtable->length);

    return NGX_OK;
}
