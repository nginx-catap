
/*
 * Copyright (C) Igor Sysoev
 */


#ifndef _NGINX_H_INCLUDED_
#define _NGINX_H_INCLUDED_


#define nginx_version         8041
#define NGINX_VERSION      "0.8.41.595.52dc8e5"
#define NGINX_VER          "nginx-catap/" NGINX_VERSION

#define NGINX_VAR          "NGINX"
#define NGX_OLDPID_EXT     ".oldbin"


#endif /* _NGINX_H_INCLUDED_ */
