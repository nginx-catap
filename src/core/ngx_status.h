
/*
 * Copyright (C) Kirill A. Korinskiy
 */


#ifndef _NGX_STATUS_H_INCLUDED_
#define _NGX_STATUS_H_INCLUDED_


#include <ngx_config.h>
#include <ngx_core.h>


typedef struct ngx_status_list_s ngx_status_list_t;

typedef struct {
#if (NGX_STATUS_TXT) || (NGX_STATUS_XML)
    ngx_str_t          caption;
    ngx_str_t          label_for_childs;
#endif

#if (NGX_STATUS_XML)
    ngx_str_t          tag_name;
    ngx_str_t          tag_value;
    ngx_str_t          tag_for_childs;
#endif

    ngx_atomic_t      *accumulate;
    ngx_atomic_t      *windows;
    ngx_status_list_t *childs;
} ngx_status_counter_t;

struct ngx_status_list_s {
    ngx_status_counter_t *counter;
    ngx_status_list_t    *next;
};

extern ngx_module_t ngx_status_module;


ngx_int_t ngx_status_add_counter(ngx_cycle_t *cycle, ngx_module_t *module,
    ngx_status_counter_t *counter);

ngx_int_t ngx_status_add_child(ngx_cycle_t *cycle, ngx_status_counter_t *parent,
    ngx_status_counter_t *child);

ngx_int_t ngx_status_fetch_add(ngx_status_counter_t *counter);

ngx_int_t ngx_status_get_accumulate_value(ngx_status_counter_t *counter);
ngx_int_t ngx_status_get_periodic_value(ngx_status_counter_t *counter,
    ngx_uint_t period);

ngx_status_list_t **ngx_status_get_counters(ngx_cycle_t *cycle);


#endif /* _NGX_STATUS_H_INCLUDED_ */
