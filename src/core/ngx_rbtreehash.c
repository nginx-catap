
/*
 * Copyright (C) Kirill A. Korinskiy
 */

#include <ngx_config.h>
#include <ngx_core.h>

typedef struct {
    u_char                      color;
    uint32_t                    crc32;

    ngx_rbtreehash_list_node_t  list_item;

    size_t      len;
    u_char      data[1];
} ngx_rbtreehash_node_t;

typedef struct {
    ngx_str_t    key;
    ngx_str_t    data;
} ngx_rbtreehash_key_t;

typedef struct {
    ngx_pool_t         *pool;	/* this pool need only for tempory using and must destroy afer create tree */
    ngx_array_t         keys;	/* keys for hash */
    ngx_rbtreehash_t   *hash;
} ngx_rbtreehash_ctx_t;

static ngx_command_t  ngx_rbtreehash_commands[] = {
    ngx_null_command
};

static ngx_core_module_t  ngx_rbtreehash_module_ctx = {
    ngx_string("rbtreehash"),
    NULL,
    NULL
};

ngx_module_t  ngx_rbtreehash_module = {
    NGX_MODULE_V1,
    &ngx_rbtreehash_module_ctx,            /* module context */
    ngx_rbtreehash_commands,               /* module directives */
    NGX_CORE_MODULE,                       /* module type */
    NULL,                                  /* init master */
    NULL,                                  /* init module */
    NULL,                                  /* init process */
    NULL,                                  /* init thread */
    NULL,                                  /* exit thread */
    NULL,                                  /* exit process */
    NULL,                                  /* exit master */
    NGX_MODULE_V1_PADDING
};

static void*
ngx_rbtreehash_alloc(ngx_rbtreehash_pool_t *pool, size_t size)
{
    if (pool->pool) {
	/* use pool to allocated memory */
	return ngx_palloc(pool->pool, size);
    }

    if (pool->shm_zone) {
	/* or shm */
	return ngx_slab_alloc((ngx_slab_pool_t *) pool->shm_zone->shm.addr,
	    size);
    }

    /* or system system memory */
    if (!pool->log) {
	/* if not set log for pool use cycle log */
	return ngx_alloc(size, ngx_cycle->log);
    }

    return ngx_alloc(size, pool->log);
}

static void
ngx_rbtreehash_free(ngx_rbtreehash_pool_t *pool, void *p)
{
    if (pool->pool) {
	/* can't have free in pool-based alloc */
	return;
    }

    if (pool->shm_zone) {
	ngx_slab_free((ngx_slab_pool_t *) pool->shm_zone->shm.addr, p);
	return;
    }

    ngx_free(p);
}


static void
ngx_rbtreehash_list_insert(ngx_rbtreehash_t *hash, ngx_rbtreehash_node_t *node)
{
    node->list_item.len = node->len;
    node->list_item.data = node->data;
    node->list_item.next = NULL;
    node->list_item.prev = NULL;

    if (hash->data->list.head) {
	hash->data->list.tail->next = &node->list_item;
	node->list_item.prev = hash->data->list.tail;
	hash->data->list.tail = &node->list_item;
    } else {
	hash->data->list.head = &node->list_item;
	hash->data->list.tail = &node->list_item;
    }
}


static void
ngx_rbtreehash_list_delete(ngx_rbtreehash_t *hash, ngx_rbtreehash_node_t *node)
{
    /* not safe, sure */

    if (&node->list_item == hash->data->list.head) {
	hash->data->list.head = hash->data->list.head->next;
	hash->data->list.head->prev = NULL;
    } else if (&node->list_item == hash->data->list.tail) {
	hash->data->list.tail = hash->data->list.tail->prev;
    } else {
	node->list_item.prev->next = node->list_item.next;
	node->list_item.next->prev = node->list_item.prev;
    }
}


static void
ngx_rbtreehash_rbtree_insert_value(ngx_rbtree_node_t *temp,
    ngx_rbtree_node_t *node, ngx_rbtree_node_t *sentinel)
{
    ngx_rbtree_node_t           **p;
    ngx_rbtreehash_node_t        *rn, *rnt;

    for ( ;; ) {

	if (node->key < temp->key) {

	    p = &temp->left;

	} else if (node->key > temp->key) {

	    p = &temp->right;

	} else { /* node->key == temp->key */

	    rn = (ngx_rbtreehash_node_t *) &node->color;
	    rnt = (ngx_rbtreehash_node_t *) &temp->color;

	    p = rn->crc32 < rnt->crc32
		? &temp->left : &temp->right;
	}

	if (*p == sentinel) {
	    break;
	}

	temp = *p;
    }

    *p = node;
    node->parent = temp;
    node->left = sentinel;
    node->right = sentinel;
    ngx_rbt_red(node);
}

ngx_rbtree_node_t* ngx_rbtreehash_insert(ngx_rbtreehash_t *hash, ngx_str_t *key,
    void *value, size_t len)
{
    uint32_t                n;
    ngx_rbtree_node_t      *node;
    ngx_rbtreehash_node_t  *rn;

    n = offsetof(ngx_rbtree_node_t, color)
	+ offsetof(ngx_rbtreehash_node_t, data)
	+ len;

    node = ngx_rbtreehash_alloc(&hash->pool, n);
    if (node == NULL) {
	return NULL;
    }
    hash->data->nodes++;

    rn = (ngx_rbtreehash_node_t*) &node->color;

    node->key = ngx_lookup3_hashlittle(key->data, key->len, 0);
    rn->crc32 = ngx_crc32_short(key->data, key->len);
    rn->len = len;
    ngx_memcpy(rn->data, value, len);

    ngx_rbtree_insert(hash->data->tree, node);
    ngx_rbtreehash_list_insert(hash, rn);

    return node;
}

ngx_int_t ngx_rbtreehash_delete(ngx_rbtreehash_t *hash, ngx_str_t *key)
{
    ngx_uint_t              hash32;
    ngx_uint_t              crc32;
    ngx_rbtree_node_t      *node;
    ngx_rbtreehash_node_t  *rn = NULL;

    hash32 = ngx_lookup3_hashlittle(key->data, key->len, 0);
    crc32 = ngx_crc32_short(key->data, key->len);
    node = hash->data->tree->root;

    while (node != hash->data->tree->sentinel) {
	if (hash32 < node->key) {
	    node = node->left;
	    continue;
	}

	if (hash32 > node->key) {
	    node = node->right;
	    continue;
	}

	do {
	    rn = (ngx_rbtreehash_node_t*) &node->color;

	    if (crc32 == rn->crc32) {
		break;
	    }

	    if (crc32 == rn->crc32) {
		break;
	    }

	    node = crc32 < rn->crc32 ? node->left : node->right;
	} while (node != hash->data->tree->sentinel && hash32 == node->key);

	break;
    }

    if (node == hash->data->tree->sentinel) {
	return NGX_OK;
    }

    ngx_rbtree_delete(hash->data->tree, node);

    ngx_rbtreehash_list_delete(hash, rn);

    ngx_rbtreehash_free(&hash->pool, node);

    hash->data->nodes--;

    return NGX_OK;
}

ngx_int_t ngx_rbtreehash_init(ngx_rbtreehash_t *hash)
{
    ngx_rbtree_node_t      *sentinel;

    hash->data = ngx_rbtreehash_alloc(&hash->pool, sizeof(ngx_rbtreehash_hash_t));
    ngx_memzero(hash->data, sizeof(ngx_rbtreehash_hash_t));

    hash->data->tree = ngx_rbtreehash_alloc(&hash->pool, sizeof(ngx_rbtree_t));
    if (hash->data->tree == NULL) {
	return NGX_ERROR;
    }

    sentinel = ngx_rbtreehash_alloc(&hash->pool, sizeof(ngx_rbtree_node_t));
    if (sentinel == NULL) {
	return NGX_ERROR;
    }

    ngx_rbtree_init(hash->data->tree, sentinel,
	ngx_rbtreehash_rbtree_insert_value);

    hash->data->list.head = NULL;
    hash->data->list.tail = NULL;

    return NGX_OK;
}

ngx_int_t ngx_rbtreehash_destroy(ngx_rbtreehash_t *hash)
{
    ngx_rbtree_node_t      *node;

    for (;;) {
	if (hash->data->tree->root == hash->data->tree->sentinel) {
	    break;
	}

	node = ngx_rbtree_min(hash->data->tree->root,
	    hash->data->tree->sentinel);

	ngx_rbtree_delete(hash->data->tree, node);

	ngx_rbtreehash_free(&hash->pool, node);

    }

    return NGX_OK;
}

static ngx_int_t
ngx_rbtreehash_init_tree(ngx_shm_zone_t *shm_zone, void *data)
{
    uint32_t                i;
    ngx_rbtreehash_t       *hash;
    ngx_rbtreehash_ctx_t   *octx = data;
    ngx_rbtreehash_ctx_t   *ctx;
    ngx_rbtreehash_key_t   *keys;

    ctx = shm_zone->data;

    if (octx &&
	ngx_strncmp(ctx->hash->pool.shm_key.data,
	    octx->hash->pool.shm_key.data,
	    ctx->hash->pool.shm_key.len) != 0) {
	ngx_log_error(NGX_LOG_EMERG, shm_zone->shm.log, 0,
	    "rbhash use path \"%s\" with previously it used "
	    "the \"%s\"", ctx->hash->pool.shm_key.data,
	    octx->hash->pool.shm_key.data);
	return NGX_ERROR;
    }

    if (ngx_rbtreehash_init(ctx->hash) != NGX_OK) {
	return NGX_ERROR;
    }

    keys = ctx->keys.elts;

    for (i = 0; i < ctx->keys.nelts; i++) {
	if (keys[i].key.len == 0) {
	    continue;
	}

	if (ngx_rbtreehash_insert(ctx->hash, &keys[i].key,
		keys[i].data.data,
		keys[i].data.len) == NULL) {
	    return NGX_ERROR;
	}

    }

    if (ctx->pool) {
	ngx_destroy_pool(ctx->pool);
	ctx->pool = NULL;
    }

    /* setup hash->data to all linked conf */
    for (hash = ctx->hash; hash->next; hash = hash->next) {
	hash->next->data = hash->data;
    }

    for (; hash->prev; hash = hash->prev) {
	hash->prev->data = hash->data;
    }

    return NGX_OK;
}

void* ngx_rbtreehash_find(ngx_rbtreehash_t *hash, ngx_str_t *key, size_t *len)
{
    ngx_uint_t              hash32;
    ngx_uint_t              crc32;
    ngx_rbtree_node_t      *node;
    ngx_rbtree_node_t      *sentinel;
    ngx_rbtreehash_node_t  *rn;

    if (!hash->data) {
	return NULL;
    }

    if (hash->data->nodes == 0) {
	return NULL;
    }

    if (key->len == 0) {
	return NULL;
    }

    hash32 = ngx_lookup3_hashlittle(key->data, key->len, 0);
    crc32 = ngx_crc32_short(key->data, key->len);

    node = hash->data->tree->root;
    sentinel = hash->data->tree->sentinel;

    while (node != sentinel) {
	if (hash32 < node->key) {
	    node = node->left;
	    continue;
	}

	if (hash32 > node->key) {
	    node = node->right;
	    continue;
	}

	do {
	    rn = (ngx_rbtreehash_node_t*) &node->color;

	    if (crc32 == rn->crc32) {
		*len = rn->len;
		return rn->data;
	    }

	    if (crc32 == rn->crc32) {
		break;
	    }

	    node = crc32 < rn->crc32 ? node->left : node->right;

	} while (node != sentinel && hash32 == node->key);
	break;
    }

    return NULL;
}

char *
ngx_rbtreehash_crete_shared_by_size(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    ngx_str_t                  *value;
    char                       *p = conf;
    size_t                      size;
    ngx_rbtreehash_ctx_t       *ctx;

    ctx = ngx_pcalloc(cf->pool, sizeof(ngx_rbtreehash_ctx_t));
    if (ctx == NULL) {
	return NGX_CONF_ERROR;
    }

    ctx->hash = (ngx_rbtreehash_t*) (p + cmd->offset);

    if (cf->args->nelts != 3) {
	return "need two args";
    }

    value = cf->args->elts;

    if (ctx->hash->pool.shm_zone) {
	return "is duplicate";
    }

    size = ngx_parse_offset(&value[2]);

    ctx->hash->pool.shm_key.len = value[1].len + NGX_INT_T_LEN;
    ctx->hash->pool.shm_key.data = ngx_palloc(cf->pool, ctx->hash->pool.shm_key.len);
    if (ctx->hash->pool.shm_key.data == NULL) {
	return NGX_CONF_ERROR;
    }

    ngx_sprintf(ctx->hash->pool.shm_key.data, "%V%d", &value[1], size);

    if (size < (size_t) (8 * ngx_pagesize)) {
	size = (size_t) (8 * ngx_pagesize);
    } else {
	size = 8 * ngx_pagesize * (size / (8 * ngx_pagesize) + 1);
    }

    ctx->hash->pool.shm_zone = ngx_shared_memory_add(cf, &ctx->hash->pool.shm_key,
	size,
	&ngx_rbtreehash_module);

    if (ctx->hash->pool.shm_zone == NULL) {
	return NGX_CONF_ERROR;
    }

    ctx->hash->pool.shm_zone->init = ngx_rbtreehash_init_tree;
    ctx->hash->pool.shm_zone->data = ctx;
    ctx->hash->pool.pool = NULL;
    ctx->hash->pool.log = cf->log;

    return NGX_CONF_OK;
}

char *
ngx_rbtreehash_from_path(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    ngx_str_t                  *value;
    char                       *p = conf;
    ngx_str_t                   data;
    ngx_str_t                   path;
    ngx_int_t                   fd;
    ngx_file_info_t             fi;
    ngx_rbtreehash_key_t       *key;
    size_t                      need_shmem = 0;
    ngx_rbtreehash_ctx_t       *ctx;
    u_char                     *ptr;
    u_char                     *ptr_last;
    u_char                     *ptr_end;
    size_t                      len;

    ctx = ngx_pcalloc(cf->pool, sizeof(ngx_rbtreehash_ctx_t));
    if (ctx == NULL) {
	return NGX_CONF_ERROR;
    }

    ctx->hash = (ngx_rbtreehash_t*) (p + cmd->offset);

    if (cf->args->nelts != 2) {
	return "need two args";
    }

    value = cf->args->elts;

    if (ctx->hash->pool.shm_zone) {
	return "is duplicate";
    }

    ctx->pool = ngx_create_pool(4096, cf->log);
    if (ctx->pool == NULL) {
	return NGX_CONF_ERROR;
    }

    if (ngx_conf_full_name(cf->cycle, &value[1], 0) == NGX_ERROR) {
	return NGX_CONF_ERROR;
    }

    path = value[1];	     /* shm_key is a path to file with data */

    fd = ngx_open_file(path.data, NGX_FILE_RDONLY, NGX_FILE_OPEN, 0);
    if (fd == NGX_INVALID_FILE) {
	if (ngx_errno == NGX_ENOENT) {
	    return NGX_CONF_OK;
	}

	ngx_log_error(NGX_LOG_CRIT, cf->log, ngx_errno,
	    ngx_open_file_n " \"%s\" failed", path.data);
	return NGX_CONF_ERROR;
    }

    if (ngx_fd_info(fd, &fi) == NGX_FILE_ERROR) {
	ngx_log_error(NGX_LOG_CRIT, cf->log, ngx_errno,
	    ngx_fd_info_n " \"%s\" failed", path.data);

	return NGX_CONF_ERROR;
    }

    data.len = ngx_file_size(&fi);
    data.data = mmap(NULL, data.len, PROT_READ, MAP_PRIVATE, fd, 0);
    if (data.data == MAP_FAILED) {
	return NGX_CONF_ERROR;
    }

    for (ptr = data.data, ptr_end = data.data + data.len, len = 0;
	 ptr <= ptr_end; ptr++) {
	if (*ptr == '\n' || ptr == ptr_end) {
	    len++;
	}
    }

    if (ngx_array_init(&ctx->keys, ctx->pool, len,
	    sizeof(ngx_rbtreehash_key_t)) != NGX_OK) {
	return NGX_CONF_ERROR;
    }

    key = ngx_array_push(&ctx->keys);
    ngx_memzero(key, ctx->keys.size);
    for (ptr_last = ptr = data.data, ptr_end = data.data + data.len
	     ; ptr < ptr_end; ptr++) {
	switch (*ptr) {
	case ' ':
	    {
		if (*ptr == *ptr_last) {
		    ptr++;
		    ptr_last = ptr;
		}
		break;
	    }
	case ':':
	    {
		if (key->key.data != NULL) {
		    continue;
		}
		key->key.len = ptr - ptr_last;
		key->key.data = ngx_palloc(ctx->pool, key->key.len);
		if (key->key.data == NULL) {
		    goto error;
		}

		memcpy(key->key.data, ptr_last, key->key.len);

		ptr++;
		ptr_last = ptr;
		break;
	    }
	case '\n':
	    {
		if (key->key.data == NULL) {
		    key->key.len = ptr - ptr_last;
		    key->key.data = ngx_palloc(ctx->pool, key->key.len);
		    if (key->key.data == NULL) {
			goto error;
		    }

		    memcpy(key->key.data, ptr_last, key->key.len);

		    key->data = key->key;
		} else {
		    key->data.len = ptr - ptr_last;
		    key->data.data = ngx_palloc(ctx->pool, key->data.len);
		    if (key->data.data == NULL) {
			goto error;
		    }

		    memcpy(key->data.data, ptr_last, key->data.len);
		}

		need_shmem += ngx_align(offsetof(ngx_rbtree_node_t, color)
		    + offsetof(ngx_rbtreehash_node_t, data)
		    + key->data.len,
		    ngx_pagesize);

		ptr++;
		ptr_last = ptr;
		key = ngx_array_push(&ctx->keys);
		ngx_memzero(key, ctx->keys.size);
		break;
	    }
	}
    }

    ctx->hash->pool.shm_key.len = path.len + NGX_INT_T_LEN;
    ctx->hash->pool.shm_key.data = ngx_palloc(cf->pool, ctx->hash->pool.shm_key.len);
    if (ctx->hash->pool.shm_key.data == NULL) {
	goto error;
    }

    /* shm_key is path to file and value hash function on the contents of file оп*/
    ngx_sprintf(ctx->hash->pool.shm_key.data, "%s %d", &path,
	ngx_lookup3_hashlittle(data.data, data.len, 0));


    munmap(data.data, data.len);

    need_shmem += ngx_align(sizeof(ngx_rbtree_t), ngx_pagesize)
	+ ngx_align(sizeof(ngx_rbtree_node_t), ngx_pagesize) /* sentinel */
	+ ngx_align(sizeof(ngx_rbtreehash_hash_t), ngx_pagesize); /* hash_data */

    if (need_shmem < (size_t) (8 * ngx_pagesize)) {
	need_shmem = (size_t) (8 * ngx_pagesize);
    } else {
	need_shmem = 8 * ngx_pagesize * (need_shmem / (8 * ngx_pagesize) + 1);
    }

    ctx->hash->pool.shm_zone = ngx_shared_memory_add(cf, &ctx->hash->pool.shm_key,
	need_shmem,
	&ngx_rbtreehash_module);

    if (ctx->hash->pool.shm_zone == NULL) {
	goto error_wo_data;
    }

    ctx->hash->pool.shm_zone->init = ngx_rbtreehash_init_tree;
    ctx->hash->pool.shm_zone->data = ctx;
    ctx->hash->pool.pool = NULL;

    return NGX_CONF_OK;

 error:
    munmap(data.data, data.len);
 error_wo_data:
    ngx_destroy_pool(ctx->pool);
    return NGX_CONF_ERROR;
}

ngx_int_t ngx_rbtreehash_merge_value(ngx_rbtreehash_t *conf, ngx_rbtreehash_t *prev)
{
    ngx_rbtreehash_t *hash;

    if (prev->pool.shm_zone || prev->pool.pool) {
	conf->pool = prev->pool;
    }

    for (hash = prev; hash->next; hash = hash->next);
    hash->next = conf;
    conf->prev = hash;

    return NGX_OK;
}

/* Local Variables: */
/* mode: c */
/* c-basic-offset: 4 */
/* c-file-offsets: ((arglist-cont-nonempty . 4)) */
/* End: */
