
/*
 * Copyright (C) Igor Sysoev
 */


#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>


typedef struct {
    ngx_uint_t                  pool_size;
    ngx_uint_t                  hash_max_size;
    ngx_uint_t                  hash_bucket_size;
} ngx_http_map_conf_t;


typedef struct {
    ngx_array_t                *key_lengths;
    ngx_array_t                *key_values;
    ngx_array_t                *key_flushes;

    ngx_uint_t                  nvars;

    ngx_hash_keys_arrays_t     *keys;

    ngx_array_t               **values_hash;

    ngx_http_variable_value_t **default_value;
    ngx_uint_t                  hostnames;      /* unsigned  hostnames:1 */
} ngx_http_map_conf_ctx_t;


typedef struct {
    ngx_hash_combined_t         hash;

    ngx_array_t                *key_lengths;
    ngx_array_t                *key_values;
    ngx_array_t                *key_flushes;

    ngx_http_variable_value_t  *default_value;
    ngx_uint_t                  hostnames;      /* unsigned  hostnames:1 */
} ngx_http_map_ctx_t;


static int ngx_libc_cdecl ngx_http_map_cmp_dns_wildcards(const void *one,
    const void *two);
static void *ngx_http_map_create_conf(ngx_conf_t *cf);
static char *ngx_http_map_block(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);
static char *ngx_http_map(ngx_conf_t *cf, ngx_command_t *dummy, void *conf);


static ngx_command_t  ngx_http_map_commands[] = {

    { ngx_string("map"),
      NGX_HTTP_MAIN_CONF|NGX_CONF_BLOCK|NGX_CONF_2MORE,
      ngx_http_map_block,
      NGX_HTTP_MAIN_CONF_OFFSET,
      0,
      NULL },

    { ngx_string("map_pool_size"),
      NGX_HTTP_MAIN_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_num_slot,
      NGX_HTTP_MAIN_CONF_OFFSET,
      offsetof(ngx_http_map_conf_t, pool_size),
      NULL },

    { ngx_string("map_hash_max_size"),
      NGX_HTTP_MAIN_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_num_slot,
      NGX_HTTP_MAIN_CONF_OFFSET,
      offsetof(ngx_http_map_conf_t, hash_max_size),
      NULL },

    { ngx_string("map_hash_bucket_size"),
      NGX_HTTP_MAIN_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_num_slot,
      NGX_HTTP_MAIN_CONF_OFFSET,
      offsetof(ngx_http_map_conf_t, hash_bucket_size),
      NULL },

      ngx_null_command
};


static ngx_http_module_t  ngx_http_map_module_ctx = {
    NULL,                                  /* preconfiguration */
    NULL,                                  /* postconfiguration */

    ngx_http_map_create_conf,              /* create main configuration */
    NULL,                                  /* init main configuration */

    NULL,                                  /* create server configuration */
    NULL,                                  /* merge server configuration */

    NULL,                                  /* create location configuration */
    NULL                                   /* merge location configuration */
};


ngx_module_t  ngx_http_map_module = {
    NGX_MODULE_V1,
    &ngx_http_map_module_ctx,              /* module context */
    ngx_http_map_commands,                 /* module directives */
    NGX_HTTP_MODULE,                       /* module type */
    NULL,                                  /* init master */
    NULL,                                  /* init module */
    NULL,                                  /* init process */
    NULL,                                  /* init thread */
    NULL,                                  /* exit thread */
    NULL,                                  /* exit process */
    NULL,                                  /* exit master */
    NGX_MODULE_V1_PADDING
};


static ngx_int_t
ngx_http_map_variable(ngx_http_request_t *r, ngx_http_variable_value_t *v,
    uintptr_t data)
{
    ngx_http_map_ctx_t  *map = (ngx_http_map_ctx_t *) data;

    ngx_str_t                   str;
    ngx_uint_t                  key;
    ngx_http_script_code_pt     code;
    ngx_http_script_engine_t    e;
    ngx_http_variable_value_t  *value;
    ngx_http_script_len_code_pt lcode;

    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                   "http map started");

    ngx_http_script_force_flush_variables(r, map->key_flushes);

    ngx_memzero(&e, sizeof(ngx_http_script_engine_t));
    e.ip = map->key_lengths->elts;
    e.request = r;
    e.flushed = 0;

    str.len = 0;

    while (*(uintptr_t *) e.ip) {
	lcode = *(ngx_http_script_len_code_pt *) e.ip;
	str.len += lcode(&e);
    }

    if (str.len == 0) {
        *v = *map->default_value;
        return NGX_OK;
    }

    str.data = ngx_palloc(r->pool, str.len);
    if (str.data == NULL) {
        return NGX_ERROR;
    }

    ngx_memzero(&e, sizeof(ngx_http_script_engine_t));
    e.pos = str.data;
    e.ip = map->key_values->elts;
    e.request = r;
    e.flushed = 0;

    while (*(uintptr_t *) e.ip) {
	code = *(ngx_http_script_code_pt *) e.ip;
	code((ngx_http_script_engine_t *) &e);
    }

    str.len = e.pos - str.data;

    if (str.len && map->hostnames && str.data[str.len - 1] == '.') {
        str.len--;
    }

    if (str.len == 0) {
        *v = *map->default_value;
        return NGX_OK;
    }

    key = ngx_hash_strlow(str.data, str.data, str.len);

    value = ngx_hash_find_combined(&map->hash, key, str.data, str.len);

    if (value) {
        *v = *value;

    } else {
        *v = *map->default_value;
    }

    ngx_log_debug2(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                   "http map: \"%V\" \"%v\"", &str, v);

    return NGX_OK;
}


static void *
ngx_http_map_create_conf(ngx_conf_t *cf)
{
    ngx_http_map_conf_t  *mcf;

    mcf = ngx_palloc(cf->pool, sizeof(ngx_http_map_conf_t));
    if (mcf == NULL) {
        return NULL;
    }

    mcf->pool_size = NGX_CONF_UNSET_UINT;
    mcf->hash_max_size = NGX_CONF_UNSET_UINT;
    mcf->hash_bucket_size = NGX_CONF_UNSET_UINT;

    return mcf;
}


static char *
ngx_http_map_block(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    ngx_http_map_conf_t  *mcf = conf;

    char                      *rv;
    ngx_str_t                 *value, name;
    ngx_conf_t                 save;
    ngx_pool_t                *pool;
    ngx_uint_t                 i, n;
    ngx_hash_init_t            hash;
    ngx_http_map_ctx_t        *map;
    ngx_http_variable_t       *var;
    ngx_http_map_conf_ctx_t    ctx;
    ngx_http_script_compile_t  sc;

    if (mcf->pool_size == NGX_CONF_UNSET_UINT) {
        mcf->pool_size = 16384;
    }

    if (mcf->hash_max_size == NGX_CONF_UNSET_UINT) {
        mcf->hash_max_size = 2048;
    }

    if (mcf->hash_bucket_size == NGX_CONF_UNSET_UINT) {
        mcf->hash_bucket_size = ngx_cacheline_size;

    } else {
        mcf->hash_bucket_size = ngx_align(mcf->hash_bucket_size,
                                          ngx_cacheline_size);
    }

    value = cf->args->elts;

    ctx.key_flushes = NULL;
    ctx.key_lengths = NULL;
    ctx.key_values = NULL;

    n = ngx_http_script_variables_count(&value[1]);

    if (n == 0) {
        return "not using a variables in map key";
    }

    ngx_memzero(&sc, sizeof(ngx_http_script_compile_t));

    sc.cf = cf;
    sc.source = &value[1];
    sc.lengths = &ctx.key_lengths;
    sc.values = &ctx.key_values;
    sc.flushes = &ctx.key_flushes;
    sc.variables = n;
    sc.complete_lengths = 1;
    sc.complete_values = 1;

    if (ngx_http_script_compile(&sc) != NGX_OK) {
	return NGX_CONF_ERROR;
    }

    ctx.nvars = cf->args->nelts - 2;

    map = ngx_pcalloc(cf->pool, sizeof(ngx_http_map_ctx_t) * ctx.nvars);
    if (map == NULL) {
        return NGX_CONF_ERROR;
    }

    ctx.keys = ngx_pcalloc(cf->pool, sizeof(ngx_hash_keys_arrays_t) * ctx.nvars);
    if (ctx.keys == NULL) {
        return NGX_CONF_ERROR;
    }

    ctx.values_hash = ngx_pcalloc(cf->pool, sizeof(ngx_array_t *) * ctx.nvars);
    if (ctx.values_hash == NULL) {
        return NGX_CONF_ERROR;
    }

    ctx.default_value = ngx_pcalloc(cf->pool,
                                    sizeof(ngx_http_variable_value_t *) * ctx.nvars);
    if (ctx.default_value == NULL) {
        return NGX_CONF_ERROR;
    }

    pool = ngx_create_pool(mcf->hash_max_size, cf->log);
    if (pool == NULL) {
        return NGX_CONF_ERROR;
    }

    for (i = 0; i < ctx.nvars; i++) {
        map[i].key_flushes = ctx.key_flushes;
        map[i].key_lengths = ctx.key_lengths;
        map[i].key_values = ctx.key_values;

        name = value[i + 2];
        name.len--;
        name.data++;

        var = ngx_http_add_variable(cf, &name, NGX_HTTP_VAR_CHANGEABLE);
        if (var == NULL) {
            ngx_destroy_pool(pool);
            return NGX_CONF_ERROR;
        }

        var->get_handler = ngx_http_map_variable;
        var->data = (uintptr_t) &map[i];

        ctx.keys[i].pool = cf->pool;
        ctx.keys[i].temp_pool = pool;

        if (ngx_hash_keys_array_init(&ctx.keys[i], NGX_HASH_LARGE) != NGX_OK) {
            ngx_destroy_pool(pool);
            return NGX_CONF_ERROR;
        }

        ctx.values_hash[i] = ngx_pcalloc(pool, sizeof(ngx_array_t) * ctx.keys[i].hsize);
        if (ctx.values_hash[i] == NULL) {
            ngx_destroy_pool(pool);
            return NGX_CONF_ERROR;
        }

        ctx.default_value[i] = NULL;
    }

    ctx.hostnames = 0;

    save = *cf;
    cf->pool = pool;
    cf->ctx = &ctx;
    cf->handler = ngx_http_map;
    cf->handler_conf = conf;

    rv = ngx_conf_parse(cf, NULL);

    *cf = save;

    if (rv != NGX_CONF_OK) {
        ngx_destroy_pool(pool);
        return rv;
    }

    for (i = 0; i < ctx.nvars; i++) {

        map[i].hostnames = ctx.hostnames;

        map[i].default_value = ctx.default_value[i] ? ctx.default_value[i]:
                                             &ngx_http_variable_null_value;

        hash.key = ngx_hash_key_lc;
        hash.max_size = mcf->hash_max_size;
        hash.bucket_size = mcf->hash_bucket_size;
        hash.name = "map_hash";
        hash.pool = cf->pool;

        if (ctx.keys[i].keys.nelts) {
            hash.hash = &map[i].hash.hash;
            hash.temp_pool = NULL;

            if (ngx_hash_init(&hash, ctx.keys[i].keys.elts, ctx.keys[i].keys.nelts)
                != NGX_OK)
                {
                    ngx_destroy_pool(pool);
                    return NGX_CONF_ERROR;
                }
        }

        if (ctx.keys[i].dns_wc_head.nelts) {

            ngx_qsort(ctx.keys[i].dns_wc_head.elts,
                      (size_t) ctx.keys[i].dns_wc_head.nelts,
                      sizeof(ngx_hash_key_t), ngx_http_map_cmp_dns_wildcards);

            hash.hash = NULL;
            hash.temp_pool = pool;

            if (ngx_hash_wildcard_init(&hash, ctx.keys[i].dns_wc_head.elts,
                                       ctx.keys[i].dns_wc_head.nelts)
                != NGX_OK)
                {
                    ngx_destroy_pool(pool);
                    return NGX_CONF_ERROR;
                }

            map[i].hash.wc_head = (ngx_hash_wildcard_t *) hash.hash;
        }

        if (ctx.keys[i].dns_wc_tail.nelts) {

            ngx_qsort(ctx.keys[i].dns_wc_tail.elts,
                      (size_t) ctx.keys[i].dns_wc_tail.nelts,
                      sizeof(ngx_hash_key_t), ngx_http_map_cmp_dns_wildcards);

            hash.hash = NULL;
            hash.temp_pool = pool;

            if (ngx_hash_wildcard_init(&hash, ctx.keys[i].dns_wc_tail.elts,
                                       ctx.keys[i].dns_wc_tail.nelts)
                != NGX_OK)
                {
                    ngx_destroy_pool(pool);
                    return NGX_CONF_ERROR;
                }

            map[i].hash.wc_tail = (ngx_hash_wildcard_t *) hash.hash;
        }

    }

    ngx_destroy_pool(pool);

    return rv;
}


static int ngx_libc_cdecl
ngx_http_map_cmp_dns_wildcards(const void *one, const void *two)
{
    ngx_hash_key_t  *first, *second;

    first = (ngx_hash_key_t *) one;
    second = (ngx_hash_key_t *) two;

    return ngx_dns_strcmp(first->key.data, second->key.data);
}


static char *
ngx_http_map(ngx_conf_t *cf, ngx_command_t *dummy, void *conf)
{
    ngx_int_t                   rc;
    ngx_str_t                  *value, file;
    ngx_uint_t                  i, j, key;
    ngx_http_map_conf_ctx_t    *ctx;
    ngx_http_variable_value_t  *var, **vp;

    ctx = cf->ctx;

    value = cf->args->elts;

    if (cf->args->nelts == 1
        && ngx_strcmp(value[0].data, "hostnames") == 0)
    {
        ctx->hostnames = 1;
        return NGX_CONF_OK;

    } else if (cf->args->nelts == 2 &&
               ngx_strcmp(value[0].data, "include") == 0) {

        file = value[1];

        if (ngx_conf_full_name(cf->cycle, &file, 1) != NGX_OK){
            return NGX_CONF_ERROR;
        }

        ngx_log_debug1(NGX_LOG_DEBUG_CORE, cf->log, 0, "include %s", file.data);

        return ngx_conf_parse(cf, &file);

    } else if (cf->args->nelts != ctx->nvars + 1) {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "invalid number of the map parameters");
        return NGX_CONF_ERROR;

    } else if (value[0].len == 0) {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "invalid first parameter");
        return NGX_CONF_ERROR;
    }


    for (i = 0; i < ctx->nvars; i++) {
        key = 0;

        for (j = 0; j < value[i + 1].len; j++) {
            key = ngx_hash(key, value[i + 1].data[i]);
        }

        key %= ctx->keys[i].hsize;

        vp = ctx->values_hash[i][key].elts;

        if (vp) {
            for (j = 0; j < ctx->values_hash[i][key].nelts; j++) {
                if (value[i + 1].len != (size_t) vp[j]->len) {
                    continue;
                }

                if (ngx_strncmp(value[i + 1].data, vp[j]->data, value[i + 1].len) == 0) {
                    var = vp[j];
                    goto found;
                }
            }

        } else {
            if (ngx_array_init(&ctx->values_hash[i][key], cf->pool, 4,
                               sizeof(ngx_http_variable_value_t *))
                != NGX_OK)
                {
                    return NGX_CONF_ERROR;
                }
        }

        var = ngx_palloc(ctx->keys[i].pool, sizeof(ngx_http_variable_value_t));
        if (var == NULL) {
            return NGX_CONF_ERROR;
        }

        var->len = value[i + 1].len;
        var->data = ngx_pstrdup(ctx->keys[i].pool, &value[i + 1]);
        if (var->data == NULL) {
            return NGX_CONF_ERROR;
        }

        var->valid = 1;
        var->no_cacheable = 1;
        var->not_found = 0;

        vp = ngx_array_push(&ctx->values_hash[i][key]);
        if (vp == NULL) {
            return NGX_CONF_ERROR;
        }

        *vp = var;

      found:

        if (ngx_strcmp(value[0].data, "default") == 0) {

            if (ctx->default_value[i]) {
                ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                                   "duplicate default map parameter");
                return NGX_CONF_ERROR;
            }

            ctx->default_value[i] = var;

            continue;
        }

        if (value[0].len && value[0].data[0] == '!') {
            value[0].len--;
            value[0].data++;
        }

        rc = ngx_hash_add_key(&ctx->keys[i], &value[0], var,
                              (ctx->hostnames) ? NGX_HASH_WILDCARD_KEY : 0);

        if (rc == NGX_OK) {
            continue;
        }

        if (rc == NGX_DECLINED) {
            ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                               "invalid hostname or wildcard \"%V\"", &value[0]);
        }

        if (rc == NGX_BUSY) {
            ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                               "conflicting parameter \"%V\"", &value[0]);
        }

        return NGX_CONF_ERROR;
    }

    return NGX_CONF_OK;
}
