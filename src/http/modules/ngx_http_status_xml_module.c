
/*
 * Copyright (C) Kirill A. Korinskiy
 */


#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>
#include <nginx.h>


#define ngx_http_status_xml_len_space(level)   (2 * (level) * (sizeof(" ") - 1))

#define ngx_http_status_xml_add_space(p, level)  { \
        ngx_int_t i;                               \
        for (i = 2 * (level); i; i--) {            \
            *(p++) = ' ';                          \
        }                                          \
    }


typedef struct {
    ngx_int_t  num;
    ngx_str_t  value;
} ngx_http_status_xml_period_t;

typedef struct {
    ngx_array_t  *periods;
    ngx_str_t     xsl_uri;
} ngx_http_status_xml_loc_conf_t;


static void *ngx_http_status_xml_create_loc_conf(ngx_conf_t *cf);
static char *ngx_http_status_xml_merge_loc_conf(ngx_conf_t *cf,
    void *parent, void *child);

static char *ngx_http_status_xml(ngx_conf_t *cf, ngx_command_t *cmd,
                                 void *conf);

static ngx_command_t  ngx_http_status_commands[] = {

    { ngx_string("status_xml"),
      NGX_HTTP_SRV_CONF|NGX_HTTP_LOC_CONF|NGX_CONF_1MORE,
      ngx_http_status_xml,
      NGX_HTTP_LOC_CONF_OFFSET,
      offsetof(ngx_http_status_xml_loc_conf_t, periods),
      NULL },

    { ngx_string("status_xml_xsl_uri"),
      NGX_HTTP_SRV_CONF|NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_str_slot,
      NGX_HTTP_LOC_CONF_OFFSET,
      offsetof(ngx_http_status_xml_loc_conf_t, xsl_uri),
      NULL },

      ngx_null_command
};


static ngx_http_module_t  ngx_http_status_xml_module_ctx = {
    NULL,                                  /* preconfiguration */
    NULL,                                  /* postconfiguration */

    NULL,                                  /* create main configuration */
    NULL,                                  /* init main configuration */

    NULL,                                  /* create server configuration */
    NULL,                                  /* merge server configuration */

    ngx_http_status_xml_create_loc_conf,   /* create location configuration */
    ngx_http_status_xml_merge_loc_conf     /* merge location configuration */
};


ngx_module_t  ngx_http_status_xml_module = {
    NGX_MODULE_V1,
    &ngx_http_status_xml_module_ctx,       /* module context */
    ngx_http_status_commands,              /* module directives */
    NGX_HTTP_MODULE,                       /* module type */
    NULL,                                  /* init master */
    NULL,                                  /* init module */
    NULL,                                  /* init process */
    NULL,                                  /* init thread */
    NULL,                                  /* exit thread */
    NULL,                                  /* exit process */
    NULL,                                  /* exit master */
    NGX_MODULE_V1_PADDING
};

static ngx_time_t   last_configure;
static ngx_str_t    last_configure_human_time;


static ngx_int_t
ngx_http_status_calcualte_len(ngx_status_list_t *c,
    ngx_http_status_xml_period_t *period, ngx_uint_t level)
{
    ngx_uint_t size = 0;

    while (c) {
        size += ngx_http_status_xml_len_space(level);
        size += sizeof("<>\n") - 1 + c->counter->tag_value.len;

	if (c->counter->tag_name.len) {
	    size += sizeof(" name=\"\"") - 1 + c->counter->tag_name.len;
	}

        size += ngx_http_status_xml_len_space(level + 1);
        size += sizeof("<caption></caption>\n") - 1 + c->counter->caption.len;
        size += ngx_http_status_xml_len_space(level + 1);
        size += sizeof("<value></value>\n") - 1 + NGX_ATOMIC_T_LEN;
        size += ngx_http_status_xml_len_space(level + 1);
        size += sizeof("<avg></avg>\n") - 1 + NGX_ATOMIC_T_LEN;
        if (c->counter->childs) {
            if (c->counter->tag_for_childs.len) {
                size += ngx_http_status_xml_len_space(level + 1);
                size += sizeof("<>\n") - 1 + c->counter->tag_for_childs.len;

                if (c->counter->label_for_childs.len) {
                    size += ngx_http_status_xml_len_space(level + 2);
                    size += sizeof("<caption></caption>\n") - 1
                        + c->counter->label_for_childs.len;
                }

                size += ngx_http_status_calcualte_len(c->counter->childs,
                                                      period, level + 2);

                size += ngx_http_status_xml_len_space(level + 1);
                size += sizeof("</>\n") - 1 + c->counter->tag_for_childs.len;
            } else {
                size += ngx_http_status_calcualte_len(c->counter->childs, period,
                                                      level + 1);
            }
        }


        size += ngx_http_status_xml_len_space(level);
        size += sizeof("</>\n") - 1 + c->counter->tag_name.len;
        c = c->next;
    }

    return size;
}


static u_char*
ngx_http_status_write_counter(u_char *p, ngx_status_list_t *c,
    ngx_http_status_xml_period_t *period, ngx_uint_t level)
{
    ngx_int_t  periodic_value;

    while (c) {
        ngx_http_status_xml_add_space(p, level);

	if (c->counter->tag_name.len) {
	    p = ngx_sprintf(p, "<%V name=\"%V\">\n",
			    &c->counter->tag_name, &c->counter->tag_value);
	} else {
	    p = ngx_sprintf(p, "<%V>\n", &c->counter->tag_name);
	}

        ngx_http_status_xml_add_space(p, level + 1);
        p = ngx_sprintf(p, "<caption>%V</caption>\n", &c->counter->caption);

        ngx_http_status_xml_add_space(p, level + 1);
        if (period->num) {
            p = ngx_sprintf(p, "<value>%uA</value>\n",
                            ngx_status_get_periodic_value(c->counter,
                                                          period->num));
        } else {
            p = ngx_sprintf(p, "<value>%uA</value>\n",
                            ngx_status_get_accumulate_value(c->counter));
        }

        periodic_value = ngx_status_get_periodic_value(c->counter,
                             period->num ? period->num : 1);

         ngx_http_status_xml_add_space(p, level + 1);
        if (period->num) {
            p = ngx_sprintf(p, "<avg>%uA</avg>\n",
                            periodic_value/period->num);
        } else {
            p = ngx_sprintf(p, "<avg>%uA</avg>\n",
                            periodic_value);
        }

        if (c->counter->childs) {
            if (c->counter->label_for_childs.len) {
                ngx_http_status_xml_add_space(p, level + 1);
                p = ngx_sprintf(p, "<%V>\n", &c->counter->tag_for_childs);

                if (c->counter->label_for_childs.len) {
                    ngx_http_status_xml_add_space(p, level + 2);
                    p = ngx_sprintf(p, "<caption>%V</caption>\n",
                                    &c->counter->label_for_childs.len);
                }

                p = ngx_http_status_write_counter(p, c->counter->childs,
                                                  period, level + 2);

                ngx_http_status_xml_add_space(p, level + 1);
                p = ngx_sprintf(p, "</%V>\n", &c->counter->tag_for_childs);

            } else {
                p = ngx_http_status_write_counter(p, c->counter->childs,
                                                  period, level + 1);
            }
        }

        ngx_http_status_xml_add_space(p, level);
        p = ngx_sprintf(p, "</%V>\n", &c->counter->tag_name);

        c = c->next;
    }

    return p;
}


static ngx_int_t ngx_http_status_xml_handler(ngx_http_request_t *r)
{
    size_t                     size;
    ngx_int_t                  rc;
    ngx_buf_t                 *b;
    ngx_uint_t                 i, j;
    ngx_chain_t                out;

    ngx_atomic_int_t           ap, hn, ac, rq, rd, wr;

    ngx_status_list_t        **counters, *c;

    ngx_http_core_srv_conf_t  *cscf;

    ngx_uint_t                 uptime, uptime_days, uptime_hours, uptime_minutes, uptime_seconds;

    ngx_http_status_xml_period_t   *period;
    ngx_http_status_xml_loc_conf_t *conf;

    if (r->method != NGX_HTTP_GET && r->method != NGX_HTTP_HEAD) {
        return NGX_HTTP_NOT_ALLOWED;
    }

    rc = ngx_http_discard_request_body(r);

    if (rc != NGX_OK) {
        return rc;
    }

    conf = ngx_http_get_module_loc_conf(r, ngx_http_status_xml_module);

    cscf = ngx_http_get_module_srv_conf(r, ngx_http_core_module);
    if (cscf == NULL) {
      return NGX_ERROR;
    }

    uptime = ngx_time() - last_configure.sec;
    uptime_days = uptime / 86400;
    uptime -= uptime_days * 86400;
    uptime_hours = uptime / 3600;
    uptime -= uptime_hours * 3600;
    uptime_minutes = uptime / 60;
    uptime -= uptime_minutes * 60;
    uptime_seconds = uptime;
    uptime = ngx_time() - last_configure.sec;


    period = conf->periods->elts;
    counters = ngx_status_get_counters((ngx_cycle_t *)ngx_cycle);

    r->headers_out.content_type.len = sizeof("text/xml") - 1;
    r->headers_out.content_type.data = (u_char *) "text/xml";

    if (r->method == NGX_HTTP_HEAD) {
        r->headers_out.status = NGX_HTTP_OK;

        rc = ngx_http_send_header(r);

        if (rc == NGX_ERROR || rc > NGX_OK || r->header_only) {
            return rc;
        }
    }

    size = sizeof("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n") - 1
           + sizeof("<ngx-status>") - 1
           + sizeof("  <server>") - 1
           + sizeof("    <ver>" NGINX_VER "</ver>") - 1
           + sizeof("    <hostmame></hostname>") - 1 + cscf->server_name.len
           + sizeof("    <reload>") - 1
           + sizeof("      <timestamp></timestamp>") - 1 + NGX_INT_T_LEN
           + sizeof("      <date></date>") - 1 + last_configure_human_time.len
           + sizeof("    </reload>") - 1
           + sizeof("    <uptime>") - 1
           + sizeof("      <seconds></seconds>") - 1 + NGX_INT_T_LEN
           + sizeof("      <time>d h m s</time>") - 1
               + NGX_INT_T_LEN + NGX_INT_T_LEN + NGX_INT_T_LEN + NGX_INT_T_LEN
           + sizeof("    </uptime>") - 1
           + sizeof("  </server>") - 1
           + sizeof("  <connections>") - 1
           + sizeof("    <active></active>") - 1 + NGX_ATOMIC_T_LEN
           + sizeof("    <accepts></accepts>") - 1 + NGX_ATOMIC_T_LEN
           + sizeof("    <handled></handled>") - 1 + NGX_ATOMIC_T_LEN
           + sizeof("    <requests></requests>") - 1 + NGX_ATOMIC_T_LEN
           + sizeof("  </connections>") - 1
           + sizeof("  <counters>") - 1;

    if (conf->xsl_uri.len) {
	size += sizeof("<?xml-stylesheet type=\"text/xsl\" href=\"\"?>") - 1 +
	    conf->xsl_uri.len;
    }

    for (i = 0; i < conf->periods->nelts; i++) {
        size += sizeof("    <period time=\"\">\n") - 1;
        if (period[i].num) {
            size += period[i].value.len;
        } else {
            size += sizeof("momentary") - 1;
        }

        for (j = 0; ngx_modules[j]; j++) {
            c = counters[ngx_modules[j]->index];

            if (c == NULL) {
                continue;
            }

            size += ngx_http_status_calcualte_len(c, &period[i], 3);
        }

        size += sizeof("    </period>\n") - 1;
    }

    size += sizeof("  </counters>") - 1
            + sizeof("</ngx-status>") - 1;

    b = ngx_create_temp_buf(r->pool, size);
    if (b == NULL) {
        return NGX_HTTP_INTERNAL_SERVER_ERROR;
    }

    out.buf = b;
    out.next = NULL;

    ap = *ngx_stat_accepted;
    hn = *ngx_stat_handled;
    ac = *ngx_stat_active;
    rq = *ngx_stat_requests;
    rd = *ngx_stat_reading;
    wr = *ngx_stat_writing;

    b->last = ngx_sprintf(b->last, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");

    if (conf->xsl_uri.len) {
	b->last = ngx_sprintf(b->last,
			      "<?xml-stylesheet type=\"text/xsl\" href=\"%V\"?>\n",
			      &conf->xsl_uri);
    }

    b->last = ngx_sprintf(b->last, "<nginx-status>\n");
    b->last = ngx_sprintf(b->last, "  <server>\n");
    b->last = ngx_sprintf(b->last, "    <ver>" NGINX_VER "</ver>\n");
    b->last = ngx_sprintf(b->last, "    <hostname>%V</hostname>\n",
                          &cscf->server_name);
    b->last = ngx_sprintf(b->last, "    <reload>\n");
    b->last = ngx_sprintf(b->last, "      <timestamp>%d</timestamp>\n",
                          last_configure.sec);
    b->last = ngx_sprintf(b->last, "      <date>%V</date>\n",
                          &last_configure_human_time);
    b->last = ngx_sprintf(b->last, "    </reload>\n");
    b->last = ngx_sprintf(b->last, "    <uptime>\n");
    b->last = ngx_sprintf(b->last, "      <seconds>%d</seconds>\n",
                          uptime);
    b->last = ngx_sprintf(b->last, "      <time>%uAd %uAh %uAm %uAs</time>\n",
                          uptime_days, uptime_hours, uptime_minutes,
                          uptime_seconds);
    b->last = ngx_sprintf(b->last, "    </uptime>\n");
    b->last = ngx_sprintf(b->last, "  </server>\n");
    b->last = ngx_sprintf(b->last, "  <connections>\n");
    b->last = ngx_sprintf(b->last, "    <active>%uA</active>\n", ac);
    b->last = ngx_sprintf(b->last, "    <accepts>%uA</accepts>\n", ap);
    b->last = ngx_sprintf(b->last, "    <handled>%uA</handled>\n", hn);
    b->last = ngx_sprintf(b->last, "    <requests>%uA</requests>\n", rq);
    b->last = ngx_sprintf(b->last, "  </connections>\n");

    b->last = ngx_sprintf(b->last, "  <counters>\n");

    for (i = 0; i < conf->periods->nelts; i++) {

        if (period[i].num) {
            b->last = ngx_sprintf(b->last, "    <period time=\"%V\">\n",
                                  &period[i].value);
        } else {
            b->last = ngx_sprintf(b->last, "    <period time=\"momentary\">\n");
        }


        for (j = 0; ngx_modules[j]; j++) {
            c = counters[ngx_modules[j]->index];

            if (c == NULL) {
                continue;
            }

            b->last = ngx_http_status_write_counter(b->last, c, &period[i], 3);
        }

        b->last = ngx_sprintf(b->last, "    </period>\n");
    }

    b->last = ngx_sprintf(b->last, "  </counters>\n");

    b->last = ngx_sprintf(b->last, "</nginx-status>");

    r->headers_out.status = NGX_HTTP_OK;
    r->headers_out.content_length_n = b->last - b->pos;

    b->last_buf = 1;

    rc = ngx_http_send_header(r);

    if (rc == NGX_ERROR || rc > NGX_OK || r->header_only) {
        return rc;
    }

    return ngx_http_output_filter(r, &out);
}


static void *
ngx_http_status_xml_create_loc_conf(ngx_conf_t *cf)
{
    ngx_http_status_xml_loc_conf_t  *conf;

    conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_status_xml_loc_conf_t));
    if (conf == NULL) {
        return NULL;
    }

    return conf;
}


static char *
ngx_http_status_xml_merge_loc_conf(ngx_conf_t *cf, void *parent, void *child)
{
    ngx_http_status_xml_loc_conf_t  *prev = parent;
    ngx_http_status_xml_loc_conf_t  *conf = child;

    if (conf->periods == NULL) {
        conf->periods = prev->periods;
    }

    ngx_conf_merge_str_value(conf->xsl_uri, prev->xsl_uri, "");

    return NGX_CONF_OK;
}


static char  *months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                           "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };


static char *ngx_http_status_xml(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    u_char       *p = conf;
    ngx_tm_t      tm;
    ngx_str_t    *value;
    ngx_uint_t    i;
    ngx_array_t **periods;

    ngx_http_core_loc_conf_t  *clcf;

    ngx_http_status_xml_period_t *period;

    periods = (ngx_array_t **) (p + cmd->offset);

    value = cf->args->elts;

    clcf = ngx_http_conf_get_module_loc_conf(cf, ngx_http_core_module);
    clcf->handler = ngx_http_status_xml_handler;

    last_configure = *ngx_cached_time;
    last_configure_human_time.len = sizeof("28/May/1987 17:30:00 +0400") - 1;
    last_configure_human_time.data = ngx_palloc(cf->pool,
                                                last_configure_human_time.len);
    if (last_configure_human_time.data == NULL) {
        return NGX_CONF_ERROR;
    }

    ngx_gmtime(ngx_cached_time->sec + ngx_cached_time->gmtoff * 60, &tm);

    (void) ngx_sprintf(last_configure_human_time.data,
                       "%02d/%s/%d %02d:%02d:%02d %c%02d%02d",
                       tm.ngx_tm_mday, months[tm.ngx_tm_mon - 1],
                       tm.ngx_tm_year, tm.ngx_tm_hour,
                       tm.ngx_tm_min, tm.ngx_tm_sec,
                       ngx_cached_time->gmtoff < 0 ? '-' : '+',
                       ngx_abs(ngx_cached_time->gmtoff / 60),
                       ngx_abs(ngx_cached_time->gmtoff % 60));

    *periods = ngx_array_create(cf->pool, cf->args->nelts - 1,
                                sizeof(ngx_http_status_xml_period_t));
    if (*periods == NULL) {
        return NGX_CONF_ERROR;
    }

    for (i = 1; i < cf->args->nelts; i++) {
        period = ngx_array_push(*periods);
        period->value = value[i];
        period->num = ngx_atoi(period->value.data, period->value.len);

        if (period->num == NGX_ERROR) {
            return NGX_CONF_ERROR;
        }
    }

    return NGX_CONF_OK;
}
